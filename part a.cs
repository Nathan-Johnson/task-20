﻿using System;

namespace task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            var num1 = 0;
            var num2 = 0;
            Console.WriteLine("Hello, Please enter a number");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter a second number");
            num2 = int.Parse(Console.ReadLine());

            if (num1 > num2)
            {   Console.WriteLine("your first number is larger than your second");
            }
            else
            {
                if(num1 < num2)
                {   Console.WriteLine("Your second number is larger than your first");
                }
                else{
                     Console.WriteLine("Your two numbers are the same");           
                }}
                Console.Clear();

            var b1 = "";
            var bLength = 0;
            Console.WriteLine("Please enter a password");
            b1 = Console.ReadLine();
            bLength = b1.Length; 
            if(bLength<8)
            {   Console.WriteLine("Your password must be a minimum of 8 Characters");
            }
            else
            {   Console.WriteLine("Well Done!");
            }
        Console.WriteLine("Press any Key to exit");
        Console.ReadKey();
        }
    }
}
